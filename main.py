from app import create_app, db

app = create_app("development")

@app.shell_context_processor
def shell_context():
    instances = {
        "app": app,
        "db": db
    }
    return instances